#!/usr/bin/env python
"""
input
access.entrayn.log.10
(4031402 lines)

1. Create access.filtered.timestamp_and_url.txt
get timestamp and url from access.entrayn.log.10
$ cat access.entrayn.log.10 | grep -oE "\[[0-9]{2}/[a-zA-Z]{3}/[0-9]{4}:[0-9]{2}:[0-9]{2}:[0-9]{2} \+0530\].* HTTP" > access.filtered.timestamp_and_url.txt

2. SCRIPT
create
    * urls
    * pd_urls
    * pd_urls_grouped

output
https://docs.google.com/spreadsheets/d/1jE1a4Gz8GyAmuvPuvjiODsZavVpTyLglgiDJqQt2gS4/edit#gid=0
"""

from datetime import datetime
import pytz
import pandas as pd
import pickle
import re


FILE = "access.filtered.timestamp_and_url.txt"


def to_datetime(nginx_timestamp_string):
    """convert 09/Oct/2017:06:55:36 to datetime.datetime(2017, 10, 9, 1, 25, 36)"""
    timestamp = datetime.strptime(
        nginx_timestamp_string, "%d/%b/%Y:%H:%M:%S"
    )
    return timestamp


# Patterns
url_patterns = [
    {
        "regex": r"^/api/v1/friend/[0-9]+\.json",
        "name": "http://entrayn.com/api/v1/friend/' + user.get('user.uid') + '.json'"
        # /api/v1/friend/49160.json
    },
    {
        "regex": r"^/api/v1/log.json",
        "name": "http://entrayn.com/api/v1/log.json"
        # /api/v1/log.json
    },
    {
        "regex": r"^/api/v1/user/[0-9]+",
        "name": "http://entrayn.com/api/v1/user/' + user.get('user.uid')"
        # /api/v1/user/110181
    },
    {
        "regex": r"^/api/v1/topic/[0-9]+/.+\.json",
        "name": "http://entrayn.com/api/v1/topic/' + user.get('user.uid') + '/' + that.attributes.exam + '.json'"
        # /api/v1/topic/18375/GRE.json
    },
    {
        "regex": r"^/api/v1/entraynuser",
        "name": "http://entrayn.com/api/v1/entraynuser"
        # /api/v1/entraynuser
    },
    {
        "regex": r"^/api/v1/system/connect\.json",
        "name": "http://entrayn.com/api/v1/system/connect.json"
        # /api/v1/system/connect.json
    },
    {
        "regex": r"^/api/v1/friend/[0-9]+",
        "name": "http://entrayn.com/api/v1/friend/' + user.get('user.uid')"
        # /api/v1/friend/122905
    },
    {
        "regex": r"^/api/v1/home/c\.json\?exam=vocab&user_id=[0-9]+",
        "name": 'http://entrayn.com/api/v1/home/c.json?exam=vocab&amp;user_id=" + user_id'
        # /api/v1/home/c.json?exam=vocab&user_id=101092
    },
    {
        "regex": r"^$",
        "name": "js/models/SET' + setNo + '.json'",
        "search": "js/models/SET"
        # NOT FOUND
    },
    {
        "regex": r"^/api/v1/user/request_password_reset\.json",
        "name": '/api/v1/user/request_password_reset.json',
        # /api/v1/home/c.json?exam=vocab&user_id=101092
    },
    {
        "regex": r"^/api/v1/user/login\.json",
        "name": 'http://entrayn.com/api/v1/user/login.json',
        # /api/v1/user/login.json
    },
    {
        "regex": r"^/file_create_url",
        "name": 'http://entrayn.com/file_create_url',
        # /file_create_url
    },
    {
        "regex": r"^/api/v1/glu/report",
        "name": 'http://entrayn.com/api/v1/glu/report',
        # /api/v1/glu/report
    },
    {
        "regex": r"^/api/v1/system/feedback",
        "name": 'http://entrayn.com/api/v1/system/feedback',
        # /api/v1/system/feedback
    },
    {
        "regex": r"^$",
        "name": "cdvfile://localhost/persistent/SET' + Util.getSid(setNo) + '.json'",
        # NOT FOUND
    },
    {
        "regex": r"^/api/v1/user/logout.json",
        "name": 'http://entrayn.com/api/v1/user/logout.json',
        # /api/v1/user/logout.json
    },
    {
        "regex": r"^/api/v1/entraynuser\.json",
        "name": 'http://entrayn.com/api/v1/entraynuser.json',
        # /api/v1/entraynuser.json
    },
    {
        "regex": r"^/api/v1/interaction\.json",
        "name": 'http://entrayn.com/api/v1/interaction.json',
        # /api/v1/interaction.json
    },
    {
        "regex": r"^/api/v1/system/word-feedback",
        "name": 'http://entrayn.com/api/v1/system/word-feedback',
        # /api/v1/system/word-feedback
    },
    {
        "regex": r"^/api/v1/question/[0-9]+\.json",
        "name": "http://entrayn.com/api/v1/question/' + Util.questionBankId + '.json'",
        # /api/v1/question/1339.json?question_count=10&no_of_groups=2&exam_type=221&subtopic=237
    },
]


urls = []
for index, line in enumerate(open(FILE), start=1):
    """
    example
    [09/Oct/2017:06:55:08 +0530] "POST /bgp-start/052b884061272cf5966a37b091693f10/82e7ad9f823f5e1b7e7af17bc5811359 HTTP
    """

    # time stamp
    nginx_access_timestamp = line.lstrip("[").split()[0]
    timestamp = to_datetime(line.lstrip("[").split()[0])

    # URL
    nginx_request_url = line.lstrip("[").split()[3]

    # Match
    is_url_match = False
    for url_pattern in url_patterns:
        if re.match(url_pattern["regex"], nginx_request_url):
            is_url_match = True

    urls.append({
        "year": timestamp.year,
        "month": timestamp.month,
        "day": timestamp.day,
        "hour": timestamp.hour,
        "minute": timestamp.minute,
        "second": timestamp.second,
        "url_actual": nginx_request_url,
        "url": url_pattern["name"] if is_url_match else "other",
        "datetime": timestamp,
        "is_url_match": is_url_match
    })

# Data Frame
pd_urls = pd.DataFrame(urls)
pickle.dump(pd_urls, open("pd_urls.p", "wb"))

# Group URLS by Date
# pd_urls_grouped = pd_urls.groupby(["year", "month", "day", "hour", "url"]).url.count().to_frame().rename(columns={"url": "request count"})

# Export to CSV
# pd_access_grouped.to_csv("urls.filtered.timestamp_and_url.csv")

# Save to file
# pickle.dump(pd_urls_grouped, open("pd_urls_grouped.p", "wb"))

# pd_access_grouped

print("create pd_urls.p")
